//
//  CategoryRepositoryTests.h
//  RecipeKing
//
//  Created by Lee Irvine on 2/17/12.
//  Copyright (c) 2012 leescode.com. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>

@class CategoryRepository;
@interface CategoryRepositoryTests : SenTestCase {
  CategoryRepository *_repository;
}

@end
