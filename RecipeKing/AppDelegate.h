//
//  AppDelegate.h
//  RecipeKing
//
//  Created by Lee Irvine on 2/4/12.
//  Copyright (c) 2012 leescode.com. All rights reserved.
//

#import <UIKit/UIKit.h>
@class NavigationController;
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, retain) NavigationController *navController;
@property (nonatomic, retain) UIWindow *window;

@end
