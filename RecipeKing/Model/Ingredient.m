//
//  Ingredient.m
//  RecipeKing
//
//  Created by Lee Irvine on 2/17/12.
//  Copyright (c) 2012 leescode.com. All rights reserved.
//

#import "Ingredient.h"
#import "Recipe.h"


@implementation Ingredient

@dynamic index;
@dynamic name;
@dynamic quantity;
@dynamic recipe;

@end
