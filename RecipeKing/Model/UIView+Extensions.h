//
//  UIView+Extensions.h
//  RecipeKing
//
//  Created by Lee Irvine on 4/15/12.
//  Copyright (c) 2012 leescode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Extensions)
- (UIView *)findFirstResponder;

@end
