//
//  Category.m
//  RecipeKing
//
//  Created by Lee Irvine on 2/17/12.
//  Copyright (c) 2012 leescode.com. All rights reserved.
//

#import "RecipeCategory.h"
#import "Recipe.h"


@implementation RecipeCategory
@dynamic name;
@dynamic recipes;
@end
