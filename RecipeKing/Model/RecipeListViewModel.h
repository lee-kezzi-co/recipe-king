//
//  RecipeListViewModel.h
//  RecipeKing
//
//  Created by Lee Irvine on 8/4/12.
//  Copyright (c) 2012 leescode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecipeListViewModel : NSObject
@property (nonatomic, retain) NSMutableArray *recipesAndCategories;
@end
