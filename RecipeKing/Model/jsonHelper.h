//
//  jsonHelper.h
//  RecipeKing
//
//  Created by Lee Irvine on 11/10/12.
//  Copyright (c) 2012 leescode. All rights reserved.
//

#import <Foundation/Foundation.h>
id valueOrNull(id o);
id valueOrNil(id o);