#import "PCategoryRepository.h"
#import "Repository.h"

@class RecipeCategory;
@interface CategoryRepository : Repository <PCategoryRepository>
@end
